<?php

module_load_include('inc', 'rsc_util', 'rsc_util.files');

/*
 * PAGE CALLBACKS
 */


function rsc_util_pages_files() {
  return [
    'heading' => [
      '#markup' => t("These pages take long to load, because they query big parts of the database and filesystem. You might have to adjust PHP's maximum execution time."),
    ],
  ];
}


function rsc_util_pages_missing_files() {

  // Get all missing files
  $missing_files = rsc_util_get_missing_files();

  // Construct a table row for each file
  $rows = [];
  foreach ($missing_files as $missing_file) {
    $file = $missing_file['file'];
    $wrapper = $missing_file['wrapper'];

    $nids = rsc_util_get_nids_using_fid($file->fid);

    // Generate links to the nodes
    $links = "";
    foreach ($nids as $nid) {
      $links .= l("Node {$nid}", "node/{$nid}") . ' ; ';
    }

    // Add it to the table of missing files
    $rows[] = [
      $file->fid,
      $file->filename,
      $wrapper->realpath(),
      $links,
    ];

  }

  $filenames = array_column($rows, 1);
  array_multisort($filenames, SORT_ASC, SORT_STRING, $rows);

  return [
    'heading' => [
      '#markup' => t('The following files are registered in the Drupal database, but are missing on the filesystem:'),
    ],
    'table' => [
      '#theme' => 'table',
      '#header' => [
        'fid',
        'filename',
        t('Expected location'),
        t('Required by'),
      ],
      '#rows' => $rows,
    ],
  ];

}


function rsc_util_pages_unmanaged_files($dir = "") {

  // disallow traversing directory tree upwards
  if (substr($dir, 0, 2) == '..') {
    $dir = "";
  }

  $rows = [];

  // From http://drupal.stackexchange.com/a/56488/8452
  if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://' . $dir)) {
    $realpath = $wrapper->realpath();

    foreach (scandir($realpath) as $entry) {
      $entrypath = "{$realpath}/{$entry}";
      if (is_dir($entrypath)) {
        $rows[] = [
          l($entry, "admin/config/media/unmanaged-files/{$entry}"),
          $entrypath,
        ];
      }
      else {

        // Test whether the file is managed by Drupal
        // TODO: also check this against the field storage (like in rsc_util_missing_files function)
        if (db_select('file_managed', 'f')
            ->fields('f')
            ->condition('uri', '%' . db_like($entry), 'LIKE')
            ->execute()
            ->rowCount() == 0) {
          $rows[] = [$entry, $entrypath];
        }

      }
    }

  }
  else {
    drupal_set_message(t('Could not determine public files location'), 'error');
  }

  return [
    'heading' => [
      '#markup' => t('The following files are present on the public filesystem, but not referenced in the Drupal database:'),
    ],
    'table' => [
      '#theme' => 'table',
      '#header' => ['name', t('full path')],
      '#rows' => $rows,
    ],
  ];

}


/**
 * @param $form
 * @param $form_state
 * @param int $limit
 *
 * @return array
 * @throws \Exception
 */
function rsc_util_acls_form($form, &$form_state, $limit = 100) {

  $node_types = node_type_get_types();

  $node_type_options = [];
  foreach ($node_types as $machine_name => $type) {
    $node_type_options[$machine_name] = $type->name;
  }

  // Default values
  $filter = isset($form_state['values']['filter_fieldset']) ? $form_state['values']['filter_fieldset'] : [
    'node_type' => NULL,
  ];

  $query = db_select('acl_node', 'an')
    ->fields('an', ['nid'])
    ->distinct()
    ->range(0, $limit);
  if (is_array($filter['node_type']) && count($filter['node_type'])) {
    $query->join(
      'node',
      'n',
      'n.nid = an.nid'
    );
    $query->condition('n.type', $filter['node_type'], 'IN');
  }
  $nids = $query->execute()->fetchCol(0);
  $nodes = node_load_multiple($nids);

  $table_header = [
    'nid' => ['data' => t('NID'), 'field' => 'n.nid'],
    'title' => ['data' => t('Title'), 'field' => 'n.title'],
    'type' => ['data' => t('Type'), 'field' => 'n.type'],
    'author' => t('Author'),
    'status' => ['data' => t('Status'), 'field' => 'n.status'],
    'changed' => [
      'data' => t('Updated'),
      'field' => 'n.changed',
      'sort' => 'desc',
    ],
  ];

  $table_options = [];
  foreach ($nodes as $node) {
    $table_options[$node->nid] = [
      'nid' => $node->nid,
      'title' => [
        'data' => [
          '#type' => 'link',
          '#title' => $node->title,
          '#href' => 'node/' . $node->nid,
          '#suffix' => ' ' . theme('mark', ['type' => node_mark($node->nid, $node->changed)]),
        ],
      ],
      'type' => check_plain(node_type_get_name($node)),
      'author' => theme('username', ['account' => $node]),
      'status' => $node->status ? t('published') : t('not published'),
      'changed' => format_date($node->changed, 'short'),
    ];
  }

  return [
    'heading' => [
      '#markup' => t("<p>The following nodes have ACLs on this site. Only the first @n are shown.</p>", [
        '@n' => $limit,
      ]),
    ],
    'filter_fieldset' => [
      '#type' => 'fieldset',
      '#title' => t('Filter nodes ...'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      'node_type' => [
        '#type' => 'select',
        '#title' => t('Content type'),
        '#options' => $node_type_options,
        '#size' => 8,
        '#required' => FALSE,
        '#multiple' => TRUE,
        '#empty_value' => NULL,
        '#default_value' => $filter['node_type'],
      ],
      'filter_button' => [
        '#type' => 'submit',
        '#value' => t('Filter'),
      ],
    ],
    'table' => [
      '#type' => 'tableselect',
      '#header' => $table_header,
      '#options' => $table_options,
      '#empty' => t('There are no ACLs to display.'),
    ],
    'remove_button' => [
      '#type' => 'submit',
      '#value' => t('Remove ACLs for selected nodes.'),
    ],
  ];

}


function rsc_util_acls_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  switch ($form_state['values']['op']) {
    case t('Remove ACLs for selected nodes.'):
      $nids = array_filter($form_state['values']['table']);
      if (empty($nids)) {
        drupal_set_message(t('No nodes were selected. Nothing was removed.'));
      }
      else {

        // Get all ACLs to delete
        $query = db_select('acl_node', 'an')
          ->fields('an', ['acl_id'])
          ->condition('an.nid', $nids, 'IN');
        $acl_ids = $query->execute()->fetchCol(0);

        // Delete them all at once instead of calling acl_delete_acl repeatedly
        db_delete('acl')
          ->condition('acl_id', $acl_ids, 'IN')
          ->execute();
        db_delete('acl_user')
          ->condition('acl_id', $acl_ids, 'IN')
          ->execute();
        db_delete('acl_node')
          ->condition('acl_id', $acl_ids, 'IN')
          ->execute();

        drupal_set_message(format_plural(count($acl_ids), 'Deleted 1 ACL.', 'Deleted @count ACLs.'));

        // For the affected nodes...
        $nodes = node_load_multiple($nids);
        $n_deleted = 0;
        foreach ($nodes as $node) {

          // Update the node access records
          node_access_acquire_grants($node);

          // Forcibly remove cached pages for this node
          $n_deleted += rsc_util_force_clear_page_cache_for_node($node);

        }

        drupal_set_message(format_plural(
          count($nodes),
          'Updated 1 node access grant.',
          'Updated @count node access grants.'
        ));

        drupal_set_message(format_plural(
          $n_deleted,
          'Cleared 1 page from the cache.',
          'Cleared @count pages from the cache.'
        ));

      }
      break;
  }
}


/**
 * Forcibly clear the page cache.
 *
 * The cache_page table contains separate records for the aliased and unaliased
 * paths of a node. Remove both.
 *
 * @param $node \Drupal\node\Entity\Node
 *    The node for which to clear the cache.
 *
 * @return int
 *    The number of records deleted from the cache_page table.
 */
function rsc_util_force_clear_page_cache_for_node($node) {

  $path = 'node/' . $node->nid;
  $url = url($path);

  $or = db_or();
  $or->condition('cid', "%$path%", 'LIKE');
  $or->condition('cid', "%$url%", 'LIKE');
  $query = db_delete('cache_page')->condition($or);
  $result = $query->execute();
  return $result;
}
